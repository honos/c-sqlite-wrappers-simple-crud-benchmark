/* 
 * File:   main.cpp
 * Author: Denis Colesnicov <eugustus@gmail.com>
 * Copyright: New BSD Licence
 * Version: 2.0
 *
 * Created on 15. září 2015, 9:04
 */

#include <time.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <algorithm>
#include <stdio.h>

/* VZHLED */
// Nazev knihovny ktera je testovana
std::string _ux_nazev_wrapper("SandBox");
// Oddelujici vizualni cary
std::string _ux_oddelujici_cara("---------------------------------------------------------");
std::string _ux_oddelujici_tecky("..................................................");


/* DATA */
// retezec pro testovani
std::string _data_retezec("Sed egestas sodales posuere. Quisque ut vulputate metus. Quisque id efficitur velit, vel mattis libero. Vivamus sapien sem, euismod non fringilla at, tincidunt et leo. Fusce ornare sagittis consectetur. Nam sit amet sagittis quam. Nunc orci mi, commodo quis erat non, volutpat tempus purus. Mauris volutpat velit a erat condimentum consectetur. Fusce est odio, finibus congue mollis et, varius id metus. Donec vitae orci nec risus lobortis ornare. Duis dolor nibh, aliquam quis neque pellentesque, posuere volutpat neque. Praesent nec quam ac lacus ultricies consectetur. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc eget laoreet metus. Sed id porttitor sapien. Nulla feugiat mi quis quam euismod blandit. Donec semper tempus leo eget tristique. Maecenas blandit viverra ex ac elementum. Quisque a magna sapien. Quisque et dictum lacus. Sed vitae tincidunt velit. Nunc aliquet magna justo, non aliquet mauris ultrices sit amet. Nunc ut metus mauris. Vestibulum sit amet metus mollis, scelerisque elit a, tempor est. Sed et lacinia nibh. Sed volutpat eleifend nisi, sed porta leo tempor id. Vivamus nec tortor justo. Integer nisl sem, finibus sit amet nunc ac, dapibus euismod magna. Suspendisse non nisi vitae eros varius luctus non a orci. Nullam feugiat blandit molestie. Quisque tempor eleifend urna non tincidunt. Pellentesque sit amet tincidunt orci. Integer sed finibus nisi. Etiam ullamcorper mattis lectus, eget mollis mi pharetra in. Donec vel fringilla orci, vitae dictum justo. Ut nec varius metus. Donec lobortis lacus justo, et vestibulum odio maximus in. Nam ac tellus quis orci molestie blandit. Aliquam risus felis, semper porta erat ac, volutpat varius felis. Vivamus condimentum aliquam tincidunt. Aenean tincidunt sapien eget purus sollicitudin, sit amet convallis quam gravida. Aliquam fringilla dolor eget nibh consequat, eu pellentesque mauris dictum. Aenean bibendum a quam vitae luctus. Quisque aliquet eget diam ac sollicitudin. Proin ultrices viverra tristique. Etiam tristique ante a nulla hendrerit fringilla. Etiam ullamcorper nunc bibendum dapibus convallis. Vivamus rutrum ullamcorper lacus id rutrum. Fusce non odio neque. Nam nulla nulla, rutrum ac neque vitae, pharetra dignissim diam. Nam placerat placerat accumsan. Ut molestie dolor id libero dapibus ornare. Nam lobortis euismod nulla non vulputate. Pellentesque euismod magna sit amet turpis luctus feugiat. Suspendisse nisl massa, interdum vitae sagittis et, volutpat vitae orci. Integer diam tellus, dignissim non nisl eget, molestie eleifend tortor. Aliquam quis tincidunt quam, id efficitur sem. Mauris sed ultricies augue. Nunc consectetur libero in risus lacinia, vitae pretium ligula varius. Ut egestas gravida enim, non dictum magna maximus vitae. Aliquam erat volutpat. Praesent aliquet leo pulvinar imperdiet egestas. Curabitur suscipit placerat turpis a condimentum. Nulla tincidunt, lacus ut euismod gravida, ex ipsum pretium ante, a ullamcorper ipsum erat ac risus. Ut hendrerit aliquam tincidunt. Donec libero ante, aliquet ac sem sed, lobortis mollis justo. Nam ac tellus quis orci molestie blandit. Aliquam risus felis, semper porta erat ac, volutpat varius felis. Viva condimentum aliquam tincidunt. Aenean tincidunt sapien eget purus sollicitudin, sit amet convallis quam gravida. Aliquam fringilla dolor eget nibh consequat, eu pellentesque mauris dictum. Aenean bibendum a quam vitae luctus. Quisque aliquet eget diam ac sollicitudin. Proin ultrices viverra tristique. Etiam tristique ante a nulla hendrerit fringilla. Vivamus rutrum ullamcorper lacus id rutrum. Nulla tortor lectus, hendrerit in nunc vel, tincidunt ullamcorper nisl. Integer suscipit ornare tellus non pulvinar. Suspendisse pulvinar odio non felis convallis, ut imperdiet nisi viverra. Ut placerat ac mauris sed placerat. Nulla iaculis aliquet leo, eu aliquam nisl fermentum vitae. Sed sit amet consectetur libero. Sed a diam purus. Sed nec blandit massa. Morbi ac pretium lectus. Vivamus vulputate ipsum purus, a congue turpis ultrices ut. Suspendisse cras amet.");
// cislice pro testovani
int _data_cislice = 1234567890;

/* NASTAVENI */
// soubor databaze SQLite
std::string _cfg_dbfile = "./db.bench.sqlite";
int _cfg_zaznamu = 10;
int _cfg_opakovani = 10;
int _rt_cikl = 0;

/**
 * Spocte rozdil mezi casy
 * 
 * @param start Zacatek mereni
 * @param konec Konec mereni
 * @return rozdil
 */
double calculateTime(const clock_t start, const clock_t konec) {
    return (double) (konec - start) / CLOCKS_PER_SEC;
}

/**
 * Otevre/vytvori databazi
 * 
 * @return 
 */
double openDB() {
    clock_t start, konec;
    start = clock();

    konec = clock();
    return calculateTime(start, konec);
}

/**
 * Vlozi radky do db
 * 
 * @return 
 */
double executeInsert() {
    clock_t start, konec;
    start = clock();

    konec = clock();
    return calculateTime(start, konec);
}

/**
 * Vybere radky z db
 * 
 * @return 
 */
double executeSelect() {
    clock_t start, konec;
    start = clock();

    konec = clock();
    return calculateTime(start, konec);
}

/**
 * Upravi radky v db
 * 
 * @return 
 */
double executeUpdate() {
    clock_t start, konec;
    start = clock();

    konec = clock();
    return calculateTime(start, konec);
}

/**
 * Odstrani radky z db
 * 
 * @return 
 */
double executeDelete() {
    clock_t start, konec;
    start = clock();

    konec = clock();
    return calculateTime(start, konec);
}

/**
 * 
 * @param argc
 * @param argv
 * @return 
 */
int main(int argc, char** argv) {

    if (argc > 1)
    {
        if (argc < 3)
        {
            printf("Argumentů musí být dva: pocet_radku_zaznamu pocet_opakovani_testu\n\n");
            return 5;
        }
        else
        {
            _cfg_zaznamu = atoi(argv[2]);
            _cfg_opakovani = atoi(argv[3]);
        }
    }

    clock_t _celkem_start, _celkem_konec;
    double _insert[_cfg_opakovani],
            _select[_cfg_opakovani],
            _update[_cfg_opakovani],
            _delete[_cfg_opakovani],
            _open,
            _t = 0;

    printf("\e[1mC++ SQLite Wrappers CRUD Benchmark - %s\e[0m\n", _ux_nazev_wrapper.c_str());
    printf("Záznamů: \e[1m%d\e[0m; Opakování: \e[1m%d\e[0m; Celkem: \e[1m%d řádků\e[0m\n", _cfg_zaznamu, _cfg_opakovani, (_cfg_zaznamu * _cfg_opakovani));
    printf("%s\n\n", _ux_oddelujici_cara.c_str());

    _celkem_start = clock();

    _open = openDB();

    for (int i = 0; i < _cfg_opakovani; i++)
    {
        _rt_cikl++;
        _insert[i] = executeInsert();
        _select[i] = executeSelect();
        _update[i] = executeUpdate();
        _delete[i] = executeDelete();
    }
    _celkem_konec = clock();

    printf("Otevřít/Vytvořit DB: \e[1m%fms\e[0m\n", _open);
    printf("\n%s\n\n", _ux_oddelujici_tecky.c_str());

    for (int i = 0; i < _cfg_opakovani; i++)
    {
        printf("Operace INSERT, opakování=%d: %f\n", i + 1, _insert[i]);
        _t += _insert[i];
    }
    printf("\nOperace \e[1mINSERT\e[0m, celkem: \e[1m%fms\e[0m \n", _t);
    printf("\n%s\n\n", _ux_oddelujici_tecky.c_str());

    _t = 0;
    for (int i = 0; i < _cfg_opakovani; i++)
    {
        printf("Operace SELECT, opakování=%d: %f\n", i + 1, _select[i]);
        _t += _select[i];
    }
    printf("\nOperace \e[1mSELECT\e[0m, celkem: \e[1m%fms\e[0m \n", _t);
    printf("\n%s\n\n", _ux_oddelujici_tecky.c_str());

    _t = 0;
    for (int i = 0; i < _cfg_opakovani; i++)
    {
        printf("Operace UPDATE, opakování=%d: %f\n", i + 1, _update[i]);
        _t += _update[i];
    }
    printf("\nOperace \e[1mUPDATE\e[0m, celkem: \e[1m%fms\e[0m \n", _t);
    printf("\n%s\n\n", _ux_oddelujici_tecky.c_str());


    _t = 0;
    for (int i = 0; i < _cfg_opakovani; i++)
    {
        printf("Operace DELETE, opakování=%d: %f\n", i + 1, _delete[i]);
        _t += _delete[i];
    }
    printf("\nOperace \e[1mDELETE\e[0m, celkem: \e[1m%fms\e[0m\n", _t);
    printf("\n%s\n\n", _ux_oddelujici_tecky.c_str());

    printf("\e[1mCelkem %f ms\e[0m\n", calculateTime(_celkem_start, _celkem_konec));

    return 0;
}











































































